//
//  ViewController.swift
//  Contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

struct Contato {
    let nome: String
    let numero:String
    let email: String
    let endereco:String
    
}
var listaContato:[Contato]=[]

class ViewController: UIViewController, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaContato.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "minhaCelula", for: indexPath) as! MyCell
        let contato = listaContato[indexPath.row]
        cell.nome.text = contato.nome
        cell.email.text = contato.email
        cell.endereco.text = contato.endereco
        cell.telefone.text = contato.numero
        return cell
    }
    
    @IBOutlet weak var TableContatos: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        TableContatos.dataSource = self
        
        listaContato.append(Contato(nome:"Cleber",numero:"31 99999-9999",email:"teste@gmail.com",endereco:"rua teste,3"))
        listaContato.append(Contato(nome:"Cleber",numero:"31 88888-8888",email:"teste2@gmail.com",endereco:"rua teste,2"))
        listaContato.append(Contato(nome:"Cleber",numero:"31 77777-7777",email:"teste1@gmail.com",endereco:"rua teste,1"))
    }


}

